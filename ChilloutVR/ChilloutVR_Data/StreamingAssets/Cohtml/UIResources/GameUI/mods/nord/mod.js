cvr.menu.prototype.nord = {
	uiRef: {},
	info: function(){
		return {
			name: "Nord",
			version_major: 1,
			version_minor: 0,
			description: "Basic Nord theme for CVR's menus.",
			author: "kzd",
			// author_id: "",
			external_links: [],
			stylesheets: [
				{filename: "vars.css", modes: ["quickmenu"]},
				{filename: "quickmenu.css", modes: ["quickmenu"]}
			],
			compatibility: [],
			icon: "",
			feature_level: 1,
			supported_modes: ["quickmenu"]
		};
	},
	register: function(menu){
		uiRef = menu;
		console.log("Nord: register");
	},
	init: function(menu){
		console.log("Nord: init");
	},
}