# cvr-nord

This is a mod made for ChilloutVR. It simply swaps some colors around to somewhat match the Nord colorscheme.

Credits to the Nord colorscheme (https://www.nordtheme.com/) and ChilloutVR. All base game code is owned by Alpha Blend Interactive.

# Examples

![Main Menu](cvr-nord-main.png)

![Quick Menu](cvr-nord-quick.png)